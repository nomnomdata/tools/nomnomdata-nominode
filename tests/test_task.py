import json

import pytest
from click.testing import CliRunner

from nomnomdata.nominode.cli import cli
from nomnomdata.nominode.task import create, delete, get, update
from nomnomdata.nominode.util import generate_random


@pytest.fixture(scope="session")
def task_uuids():
    project_uuid = "HGH1J-YOP1M"
    config = {
        "project_uuid": project_uuid,
        "uuid": generate_random(11),
    }
    runner = CliRunner()
    # Delete task if exists to initialize
    runner.invoke(
        delete,
        args=f"""-tu {config["uuid"]} -pu {config["project_uuid"]} """,
        catch_exceptions=False,
    )
    yield config

    runner.invoke(
        delete,
        args=f"""-tu {config["uuid"]} -pu {config["project_uuid"]} """,
        catch_exceptions=False,
    )


def test_help():
    runner = CliRunner()
    result = runner.invoke(cli, "--help")
    assert result.exit_code == 0


def test_task_file_operations(task_uuids):
    runner = CliRunner()
    path = "tests/sample/task1_v{x}.json"
    task_uuid = "TESTS-UUIDS"
    runner.invoke(
        delete,
        args=f"""-tu {task_uuid} -pu {task_uuids["project_uuid"]} """,
        catch_exceptions=False,
    )

    # Create from file
    runner = CliRunner()
    result = runner.invoke(
        create, args=f""" -f '{path.format(x='1')}'""", catch_exceptions=False
    )
    result = json.loads(result.output)
    task = result["results"]

    assert result["code"] == 202
    assert task["uuid"] == task_uuid
    assert task["project_uuid"] == task_uuids["project_uuid"]
    assert task["parameters"]["seconds"] == 30

    # Update from file
    result = runner.invoke(
        update, args=f""" -f '{path.format(x='2')}'""", catch_exceptions=False
    )
    result = json.loads(result.output)
    task = result["results"]
    assert task["parameters"]["seconds"] == 90
    assert task["parameters"]["alias"] == "Junk Test2"

    # Update from file with excludes
    result = runner.invoke(
        update, args=f""" -f '{path.format(x='3')}'""", catch_exceptions=False
    )
    result = json.loads(result.output)
    task = result["results"]
    assert task["parameters"]["seconds"] == 90
    assert task["parameters"]["alias"] == "Junk Test"
    assert task["options"] == {"channel": "stable"}
    assert (
        task["parameters"]["sql_step"]
        == "SELECT *\nFROM mytest\nWHERE  {{ mydate }} >= {{ test }};\n"
    )
    assert task["parameters"]["double"] == 2

    runner.invoke(
        delete,
        args=f"""-tu {task_uuid} -pu {task_uuids["project_uuid"]} """,
        catch_exceptions=False,
    )


def test_task_create_string(task_uuids):
    test_task = {
        "uuid": task_uuids["uuid"],
        "project_uuid": task_uuids["project_uuid"],
        "alias": "Junk Test",
        "engine_uuid": "df6872b9-e8bd-11ea-acb4-0e89e1759f15",
        "options": {"channel": "stable"},
        "parameters": {
            "action_name": "wait_seconds",
            "alias": "Junk Test",
            "seconds": 30,
        },
    }
    runner = CliRunner()

    result = runner.invoke(
        create, args=f""" -s '{json.dumps(test_task)}'""", catch_exceptions=False
    )
    result = json.loads(result.output)
    task = result["results"]

    assert result["code"] == 202
    assert task["uuid"] == task_uuids["uuid"]
    assert task["project_uuid"] == task_uuids["project_uuid"]
    assert task["parameters"]["seconds"] == 30

    # test Skipping of existing task
    test_task["parameters"]["seconds"] = 40

    result = runner.invoke(
        create, args=f""" -s '{json.dumps(test_task)}'""", catch_exceptions=False
    )
    result = json.loads(result.output)
    assert result["code"] == 400
    assert result["message"] == "Task already exists."
    # test overwrite of existing task
    result = runner.invoke(
        create,
        args=f""" -s '{json.dumps(test_task)}' --overwrite""",
        catch_exceptions=False,
    )
    result = json.loads(result.output)
    task = result["results"]
    assert result["code"] == 202
    assert task["parameters"]["seconds"] == 40


def test_task_get_string(task_uuids):
    runner = CliRunner()
    result = runner.invoke(
        get,
        args=f"""-tu {task_uuids["uuid"]} -pu {task_uuids["project_uuid"]} """,
        catch_exceptions=False,
    )
    result = json.loads(result.output)
    task = result["results"]
    assert result["code"] == 200
    assert task["uuid"] == task_uuids["uuid"]
    assert task["project_uuid"] == task_uuids["project_uuid"]

    result = runner.invoke(
        get,
        args=f"""-tu DOESNOTEXIST -pu {task_uuids["project_uuid"]} """,
        catch_exceptions=False,
    )
    result = json.loads(result.output)
    assert result["code"] == 404
    assert result["message"] == "Unable to locate task `DOESNOTEXIST`"


def test_task_update_string(task_uuids):

    runner = CliRunner()
    result = runner.invoke(
        get,
        args=f"""-tu {task_uuids["uuid"]} -pu {task_uuids["project_uuid"]} """,
        catch_exceptions=False,
    )
    result = json.loads(result.output)
    task = result["results"]
    assert result["code"] == 200
    assert task["uuid"] == task_uuids["uuid"]
    assert task["project_uuid"] == task_uuids["project_uuid"]
    assert task["alias"] == "Junk Test"
    assert task["parameters"]["seconds"] == 40

    test_task = {
        "alias": "Junk BKDS:EKD",
        "engine_uuid": "df6872b9-e8bd-11ea-acb4-0e89e1759f15",
        "project_uuid": task_uuids["project_uuid"],
        "options": {"channel": "dev"},
        "uuid": task_uuids["uuid"],
        "parameters": {
            "action_name": "wait_seconds",
            "seconds": 60,
            "pointer": "2020-01-01",
        },
    }

    # Basic Update
    result = runner.invoke(
        update, args=["-s", json.dumps(test_task)], catch_exceptions=False
    )
    result = json.loads(result.output)
    task = result["results"]
    assert result["code"] == 202
    assert task["uuid"] == task_uuids["uuid"]
    assert task["alias"] == "Junk BKDS:EKD"
    assert task["project_uuid"] == task_uuids["project_uuid"]
    assert task["options"]["channel"] == "dev"
    assert task["parameters"]["seconds"] == 60
    assert task["parameters"]["action_name"] == "wait_seconds"

    # No Changes should exclude updating alias and parameters
    test_task["parameters"]["seconds"] = 40
    test_task["alias"] = "Junk Test"
    test_task["_deploy"] = {"exclude": ["alias", "parameters"]}
    result = runner.invoke(
        update, args=["-s", json.dumps(test_task)], catch_exceptions=False,
    )
    result = json.loads(result.output)
    task = result["results"]
    assert result["code"] == 200
    assert task["parameters"]["seconds"] == 60
    assert task["alias"] == "Junk BKDS:EKD"
    assert task["parameters"]["action_name"] == "wait_seconds"

    # NESTED testing exclude parameters seconds
    test_task["parameters"]["seconds"] = 65
    test_task["parameters"]["action_name"] = "boom"
    test_task["alias"] = "Junk Test BLDKDF"
    test_task["_deploy"] = {"exclude": ["parameters.seconds"]}
    result = runner.invoke(
        update, args=["-s", json.dumps(test_task)], catch_exceptions=False,
    )
    result = json.loads(result.output)
    task = result["results"]
    assert result["code"] == 202
    assert task["parameters"]["seconds"] == 60  # The excluded one
    assert task["alias"] == "Junk Test BLDKDF"
    assert task["parameters"]["action_name"] == "boom"

    # Dry Run - Nothing should change
    test_task["parameters"]["seconds"] = 100
    test_task.pop("_deploy")
    result = runner.invoke(
        update, args=["-s", json.dumps(test_task), "--dry-run"], catch_exceptions=False,
    )
    result = json.loads(result.output)
    assert result["code"] == 202
    assert result["status"] == "Dry Run"
    assert task["parameters"]["seconds"] == 60


def test_task_delete_string(task_uuids):
    runner = CliRunner()
    result = runner.invoke(
        delete, args=f"""-tu {task_uuids["uuid"]} -pu {task_uuids["project_uuid"]} """
    )
    result = json.loads(result.output)
    assert result["code"] in [202]
    assert (
        result["message"]
        == f"""Task identified by uuid: {task_uuids["uuid"]} successfully deleted"""
    )

    result = runner.invoke(
        delete, args=f"""-tu {task_uuids["uuid"]} -pu {task_uuids["project_uuid"]} """
    )
    result = json.loads(result.output)
    assert result["code"] in [404]
    assert (
        result["message"]
        == f"""Unable to locate task identified by uuid: {task_uuids["uuid"]}"""
    )
