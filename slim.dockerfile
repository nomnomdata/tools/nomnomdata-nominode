FROM python:3.7 as builder
RUN pip install poetry dunamai

RUN mkdir /tool-source

COPY ./.git /tool-source/.git
COPY ./poetry.lock /tool-source
COPY ./pyproject.toml /tool-source
COPY ./README.md /tool-source
COPY ./nomnomdata /tool-source/nomnomdata

WORKDIR /tool-source
RUN poetry version $(dunamai from any --no-metadata --style pep440)
RUN pip wheel --wheel-dir /python-wheels /tool-source

FROM python:3.7-slim

COPY --from=builder python-wheels /python-wheels
RUN pip install --no-index --find-links /python-wheels /python-wheels/* && rm -rf /python-wheels

CMD nnd engine run pkg/executable.py
